export const state = () => ({
    token: false
})

export const mutations = {
    setToken(state, token) {
        state.token = token
    },
    crearToken(state) {
        state.token = null
}
}

export const actions = {
    async login ({commit, dispatch}, formData) {
        try {
            const token = await new Promise((resolve, reject) => {
                setTimeout(() => resolve('mock-token'), 2000)
            });
            commit('setToken', token);
        } catch(e) {
            commit("setError", e, {root: true})
            throw new Error(e);
        }
    },

    async createUser({commit}, formData) {
        try {
            console.log('Create User', formData);
        } catch (error) {
          throw new Error(e);
        }
    },

    logout({commit}) {
        commit('crearToken');
    },
}

export const getters = {
    isAuthenticated: state => Boolean(state.token)
}
