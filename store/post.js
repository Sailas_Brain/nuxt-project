const posts = [
    {title: 'Post', date: new Date(), views: 22, comments: [1,2,3], _id: 'id1'},
    {title: 'Post 1', date: new Date(), views: 23, comments: [1,2,3], _id: 'id2'},
]
export const actions = {
    async fetchAdmin ({commit}) {
        return await new Promise( resolve => {
            setTimeout(() => {
                resolve(posts)
            }, 1000)
        });
    },

    async remove({commit}, id) {

    },

    async update({commit}, {id, text}) {

    },

    async create({commit}, {title, text, image}) {
        try {
            const fd = new FormData()
            fd.append('title', title)
            fd.append('text', text)
            fd.append('image', image, image.name)
            return await new Promise( resolve => {
                setTimeout(() => resolve(), 1000)
            });
        } catch (e) {
            commit('setError', e, {root: true})
            throw new Error(e);
        }
    },

    async fetchAdminById({commit}, id) {
        return await new Promise( resolve => {
            setTimeout(() => {
                resolve(posts.find(p => p._id === id));
            }, 1000)
        });
    }
}
